// Query the Consul catalog for the name service and enumerate over it
package main

import (
	"fmt"
	"net"
	"os"
	"strings"

	"github.com/hashicorp/consul/api"
)

func main() {
	client, err := api.NewClient(api.DefaultConfig())
	check(err)

	services, meta, err := client.Catalog().Service("Name", "", nil)
	check(err)

	if (meta.LastIndex == 0) || (len(services) == 0) {
		check("Name service not found")
	}

	myip, err := getLocalIP()
	check(err)

	for _, a := range services {
		if myip == a.Address {
			fmt.Printf("%s%02d\n", strings.Join(a.ServiceTags, "."), len(services))
			os.Exit(0)
		}
	}

	check(myip)
}

func getLocalIP() (string, error) {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return "", err
	}
	for _, address := range addrs {
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String(), nil
			}
		}
	}

	return "", err
}

func check(err interface{}) {
	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}
}
