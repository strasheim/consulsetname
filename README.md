
## Generating a name from Consul 

For autoscaling groups the new AMIs should have a meaningful name, in case a human needs to get there. 
It's also helpful for alerts and metrics. 

ConsulSetName is looking into consul to generate a name and add a number to that name. It does print out the name to stdout. 
Therefore setting the name and updating the name is not part of the tool. 

#### Register a fake service in consul 

To make this work it searches for an entry like this: 
```
{ "service": { "name":"Name", "tags": ["env01","apache"] } } 
```
     
This would result in the name: env01.apacheXX - the XX would be the new number which is one higher than the already present services.

#### How to install

1. set GOPATH
2. go get -u gitlab.com/strasheim/consulsetname
3. binary is under $GOPATH/bin/

#### Example

The JSON above will work and produce running numbers only if you have a second service on the system registered that is called **apache**
If you get somename 00 or just 00 it means your service names are not matching and it couldn't find the relation. 
 
```
/usr/local/bin/consulsetname
env01.apache02
```

